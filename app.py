from flask import Flask,render_template, request


app = Flask(__name__)

perguntas = ['P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7', 'P8', 'P9', 'A1', 'A2', 'A3', 'A4', 'A5', 'A6',
 'A7', 'A8', 'CL1', 'CL2', 'CL3', 'CL4', 'CL5', 'CL6', 'CL7', 'CO1', 'CO2', 'CO3', 'CO4', 'CO5', 'I1', 'I2', 'I3', 'I4', 'I5', 'I6', 'I7', 'I8']

@app.route('/')
def index():
    topicos = ["1.Professor",'2.aluno - auto avalização']
    # perguntas = ['foi claro','depertou','preocupou','planejou']
    return render_template('questionario.html',topicos = topicos,perguntas = perguntas)
    
    
app.run()

# {'p1':'valor1','p2':'valor'}